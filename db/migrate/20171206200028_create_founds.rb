class CreateFounds < ActiveRecord::Migration[5.1]
  def change
    create_table :founds do |t|
      t.string :name
      t.string :phone_number
      t.string :picture
      t.text :description

      t.timestamps
    end
  end
end
