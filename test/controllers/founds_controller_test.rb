require 'test_helper'

class FoundsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get founds_new_url
    assert_response :success
  end

  test "should get show" do
    get founds_show_url
    assert_response :success
  end

end
