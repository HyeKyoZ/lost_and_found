Rails.application.routes.draw do
  root 'founds#index'

  get 'search', to: 'search#search'

  resources :founds, only: %i( show new  create )
end
