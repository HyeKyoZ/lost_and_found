class SearchController < ApplicationController
  def search
    @q = Found.ransack(params[:q])
    @results = @q.result
  end
end
