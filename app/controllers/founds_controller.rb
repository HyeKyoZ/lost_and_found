class FoundsController < ApplicationController

  def index
    @founds = Found.all
    @q = Found.ransack
  end

  def new
    @found = Found.new
  end

  def create
    @found = Found.new found_params
    if @found.save
      redirect_to @found, notice: '失物提交成功！'
    else
      render :new
    end
  end

  def show
    @found = Found.find params[:id]
  end

  private

    def found_params
      params.require(:found).permit(:name, :phone_number, :description, :picture)
    end
end
