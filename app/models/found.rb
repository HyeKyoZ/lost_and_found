class Found < ApplicationRecord
  mount_uploader :picture, PictureUploader

  validates :name, presence: true
  validates :phone_number, presence: true
end
